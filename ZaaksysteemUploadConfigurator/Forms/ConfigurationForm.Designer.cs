﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

namespace ZaaksysteemUploadConfigurator
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label1;
            this.saveButton = new System.Windows.Forms.Button();
            this.folderPickButton = new System.Windows.Forms.Button();
            this.selectedFolder = new System.Windows.Forms.Label();
            this.apiKeyEntry = new System.Windows.Forms.TextBox();
            this.zaaksysteemUrlEntry = new System.Windows.Forms.TextBox();
            this.configurationNameEntry = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(12, 41);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(95, 13);
            label2.TabIndex = 2;
            label2.Text = "Zaaksysteem-URL";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(12, 67);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(44, 13);
            label3.TabIndex = 1;
            label3.Text = "API-key";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(12, 95);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(61, 13);
            label4.TabIndex = 0;
            label4.Text = "Uploadmap";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(12, 15);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(35, 13);
            label1.TabIndex = 7;
            label1.Text = "Naam";
            // 
            // saveButton
            // 
            this.saveButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.saveButton.Location = new System.Drawing.Point(368, 139);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 5;
            this.saveButton.Text = "Opslaan";
            this.saveButton.UseVisualStyleBackColor = true;
            // 
            // folderPickButton
            // 
            this.folderPickButton.Location = new System.Drawing.Point(113, 90);
            this.folderPickButton.Name = "folderPickButton";
            this.folderPickButton.Size = new System.Drawing.Size(75, 23);
            this.folderPickButton.TabIndex = 4;
            this.folderPickButton.Text = "Kies folder...";
            this.folderPickButton.UseVisualStyleBackColor = true;
            this.folderPickButton.Click += new System.EventHandler(this.folderPickButton_Click);
            // 
            // selectedFolder
            // 
            this.selectedFolder.AutoSize = true;
            this.selectedFolder.Location = new System.Drawing.Point(194, 95);
            this.selectedFolder.Name = "selectedFolder";
            this.selectedFolder.Size = new System.Drawing.Size(12, 13);
            this.selectedFolder.TabIndex = 3;
            this.selectedFolder.Text = "x";
            this.selectedFolder.TextChanged += new System.EventHandler(this.selectedFolder_TextChanged);
            // 
            // apiKeyEntry
            // 
            this.apiKeyEntry.Location = new System.Drawing.Point(113, 64);
            this.apiKeyEntry.Name = "apiKeyEntry";
            this.apiKeyEntry.Size = new System.Drawing.Size(330, 20);
            this.apiKeyEntry.TabIndex = 3;
            // 
            // zaaksysteemUrlEntry
            // 
            this.zaaksysteemUrlEntry.Location = new System.Drawing.Point(113, 38);
            this.zaaksysteemUrlEntry.Name = "zaaksysteemUrlEntry";
            this.zaaksysteemUrlEntry.Size = new System.Drawing.Size(330, 20);
            this.zaaksysteemUrlEntry.TabIndex = 2;
            // 
            // configurationNameEntry
            // 
            this.configurationNameEntry.Location = new System.Drawing.Point(113, 12);
            this.configurationNameEntry.Name = "configurationNameEntry";
            this.configurationNameEntry.Size = new System.Drawing.Size(330, 20);
            this.configurationNameEntry.TabIndex = 1;
            this.configurationNameEntry.TextChanged += new System.EventHandler(this.configurationNameEntry_TextChanged);
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 174);
            this.Controls.Add(label1);
            this.Controls.Add(this.configurationNameEntry);
            this.Controls.Add(label4);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(this.zaaksysteemUrlEntry);
            this.Controls.Add(this.apiKeyEntry);
            this.Controls.Add(this.selectedFolder);
            this.Controls.Add(this.folderPickButton);
            this.Controls.Add(this.saveButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::ZaaksysteemUploadConfigurator.Properties.Resources.zaaksysteem;
            this.MaximizeBox = false;
            this.Name = "ConfigurationForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configuratie";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button folderPickButton;
        public System.Windows.Forms.Label selectedFolder;
        public System.Windows.Forms.TextBox apiKeyEntry;
        public System.Windows.Forms.TextBox zaaksysteemUrlEntry;
        public System.Windows.Forms.TextBox configurationNameEntry;
    }
}

