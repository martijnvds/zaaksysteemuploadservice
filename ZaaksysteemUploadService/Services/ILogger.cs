﻿#region Software License
/*
    Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System.Diagnostics;

namespace ZaaksysteemUploadService
{
    public interface ILogger
    {
        void WriteEntry(string message);
        void WriteEntry(string message, EventLogEntryType type);
        void DebugEntry(string message);
    }
}
