﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ZaaksysteemUpload
{
    [DataContract]
    public class ConfigurationDataItem : IEquatable<ConfigurationDataItem>
    {
        [DataMember(IsRequired = true)]
        public string Name { get; set; }

        [DataMember(IsRequired = true)]
        public string APIKey { get; set; }

        [DataMember(IsRequired = true)]
        public string PostURL { get; set; }

        [DataMember(IsRequired = true)]
        public string WatchPath { get; set; }

        [DataMember(IsRequired = true)]
        public bool Valid { get; set; }

        public string DisplayName {
            get
            {
                if (Valid) {
                    return Name;
                }
                else {
                    return String.Format("{0} (ongeldig)", Name);
                }
            }
        }

        #region Equality methods

        public bool Equals(ConfigurationDataItem other)
        {
            if (other == null) return false;

            return string.Equals(Name, other.Name)
                && string.Equals(APIKey, other.APIKey)
                && string.Equals(PostURL, other.PostURL)
                && string.Equals(WatchPath, other.WatchPath)
                && Valid == other.Valid;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals(obj as ConfigurationDataItem);
        }

        public override int GetHashCode()
        {
            return new { Name, APIKey, PostURL, WatchPath, Valid }.GetHashCode();
        }

        #endregion
    }

    [DataContract]
    public class ConfigurationData
    {
        [DataMember(IsRequired = true)]
        public List<ConfigurationDataItem> ConfigurationItems;

        [DataMember(IsRequired = true)]
        public uint HTTPTimeoutMinutes { get; set; }
    }
}
